package price.per.ounce;

import java.math.BigDecimal;
import java.math.RoundingMode;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

public class PricePerOunce {

    FirebaseAnalytics mFirebaseAnalytics;

    public EditText totalPriceText, numberOfOuncesText, pricePerOunceText;
    public Button convertButton, clearButton;
    public FragmentActivity activity;
    private Toast toast;

    // get edit text and buttons
    public void addViews(View v) {
        totalPriceText = (EditText) v.findViewById(R.id.total_price);
        totalPriceText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                pricePerOunceText.setText("");
            }
        });

        numberOfOuncesText = (EditText) v.findViewById(R.id.number_of_ounces);
        numberOfOuncesText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pricePerOunceText.setText("");
            }
        });

        pricePerOunceText = (EditText) v
                .findViewById(R.id.price_per_ounce_edittext);

        convertButton = (Button) v.findViewById(R.id.convert_button);
        convertButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                convertButton(v);
            }
        });

        clearButton = (Button) v.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clearButton(v);
            }
        });
    }

    protected void convertButton(View v) {
        Editable totalPrice, numberOfOunces;

        totalPrice = totalPriceText.getText();
        numberOfOunces = numberOfOuncesText.getText();

        // valid convert has numbers if total price and number of ounces have a
        // value
        boolean hasValidValues;
        hasValidValues = checkValidTotalAndOunces(totalPrice, numberOfOunces);

        if (!hasValidValues) {
            showToast(activity.getBaseContext(), "Please check values for valid total and ounce count");
        } else {
            // price per ounce text is updated from the total price and price
            // per ounce
            String t = totalPrice.toString();
            BigDecimal total = new BigDecimal(t);

            String n = numberOfOunces.toString();
            BigDecimal number = new BigDecimal(n);

            BigDecimal price = total
                    .divide(number, 2, RoundingMode.HALF_UP);

            totalPriceText.setText(total.toString());
            numberOfOuncesText.setText(number.toString());
            pricePerOunceText.setText(price.toString());

            Bundle params = new Bundle();
            params.putString("conversion", "conversion successful");
            mFirebaseAnalytics.logEvent("conversion", params);
        }
    }

    private boolean checkValidTotalAndOunces(Editable totalPrice,
                                             Editable numberOfOunces) {
        boolean hasValues, hasValidValues = false;

        hasValues = totalPrice.length() > 0 && numberOfOunces.length() > 0;
        if (hasValues) {
            String t = totalPrice.toString();
            String n = numberOfOunces.toString();

            try {
                BigDecimal total = new BigDecimal(t);
                BigDecimal number = new BigDecimal(n);
                hasValidValues = (!total.equals(BigDecimal.ZERO)) && (!number.equals(BigDecimal.ZERO));
            }catch (NumberFormatException nfe){
                return false;
            }
        }

        return hasValues && hasValidValues;
    }

    protected void clearButton(View v) {
        // if any fields have text in them return to the hint state
        Editable s;
        s = totalPriceText.getText();
        if (s.length() > 0)
            totalPriceText.setText("");

        s = numberOfOuncesText.getText();
        if (s.length() > 0)
            numberOfOuncesText.setText("");

        s = pricePerOunceText.getText();
        if (s.length() > 0)
            pricePerOunceText.setText("");
    }

    public void addActivity(FragmentActivity act) {
        activity = act;

        //record evaluation
        // [START shared_app_measurement]
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        // [END shared_app_measurement]
    }

    public void showToast(Context cont, String message) {
        // create if not, or set text to it
        if (toast == null) {
            toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
        }
        if (!toast.getView().isShown()) {
            toast.setText(message);
            toast.show();
        } else {
            toast.setText(message);
        }
    }
}