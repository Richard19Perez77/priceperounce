package price.per.ounce;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.firebase.analytics.FirebaseAnalytics;

public class PricePerOunceFragment extends Fragment {

	/**
	 * The {@code FirebaseAnalytics} used to record screen views.
	 */
	// [START declare_analytics]
	private FirebaseAnalytics mFirebaseAnalytics;
	// [END declare_analytics]

	public PricePerOunce convert;
	private String TOTAL_PRICE = "totalPrice";
	private String NUMBER_OF_OUNCES = "numberOfOunces";
	private String PRICE_PER_OUNCE = "pricePerOunce";

	public PricePerOunceFragment() {
		convert = new PricePerOunce();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// [START shared_app_measurement]
		// Obtain the FirebaseAnalytics instance.
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
		// [END shared_app_measurement]
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			convert.totalPriceText.setText(savedInstanceState
					.getCharSequence(TOTAL_PRICE + this.getTag()));
			convert.numberOfOuncesText.setText(savedInstanceState
					.getCharSequence(NUMBER_OF_OUNCES + this.getTag()));
			convert.pricePerOunceText.setText(savedInstanceState
					.getCharSequence(PRICE_PER_OUNCE + this.getTag()));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putCharSequence(TOTAL_PRICE + this.getTag(),
				convert.totalPriceText.getText().toString());
		outState.putCharSequence(NUMBER_OF_OUNCES + this.getTag(),
				convert.numberOfOuncesText.getText().toString());
		outState.putCharSequence(PRICE_PER_OUNCE + this.getTag(),
				convert.pricePerOunceText.getText().toString());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_priceperounce_layout, container, false);
		convert.addViews(rootView);
		convert.addActivity(getActivity());

		SharedPreferences sharedPref = getActivity().getPreferences(
				Context.MODE_PRIVATE);

		convert.totalPriceText.setText(sharedPref.getString(
				TOTAL_PRICE + this.getTag(), ""));
		convert.numberOfOuncesText.setText(sharedPref.getString(
				NUMBER_OF_OUNCES + this.getTag(), ""));
		convert.pricePerOunceText.setText(sharedPref.getString(
				PRICE_PER_OUNCE + this.getTag(), ""));

		Animation anim = AnimationUtils.loadAnimation(getActivity().getBaseContext(),
				R.anim.intro);
		anim.reset();
		rootView.clearAnimation();
		rootView.startAnimation(anim);

		return rootView;
	}

	@Override
	public void onStop() {
		super.onStop();
		SharedPreferences sharedPref = getActivity().getPreferences(
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(TOTAL_PRICE + this.getTag(), convert.totalPriceText
				.getText().toString());
		editor.putString(NUMBER_OF_OUNCES + this.getTag(),
				convert.numberOfOuncesText.getText().toString());
		editor.putString(PRICE_PER_OUNCE + this.getTag(),
				convert.pricePerOunceText.getText().toString());
		editor.apply();
	}
}