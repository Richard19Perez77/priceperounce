package price.per.ounce;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

public class ApplicationConvertTest extends
        ActivityInstrumentationTestCase2<MainActivity> {

    static final String TAG = "priceTests";

    MainActivity mainActivity;
    PricePerOunce convert;
    PricePerOunceFragment fragment;
    ValidDate validData = new ValidDate();
    InValidData invalidData = new InValidData();
    NonTerminatingData nonTerminatingData = new NonTerminatingData();

    EditText priceEditText = null;
    EditText totalOunces = null;
    EditText pricePerOunceText = null;
    Button clearButton = null;
    Button convertButton = null;

    public ApplicationConvertTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        fragment = (PricePerOunceFragment) getActivity()
                .getSupportFragmentManager().findFragmentByTag(
                        "pricePerOunceTag");
        convert = fragment.convert;
        priceEditText = convert.totalPriceText;
        totalOunces = convert.numberOfOuncesText;
        pricePerOunceText = convert.pricePerOunceText;
        clearButton = convert.clearButton;
        convertButton = convert.convertButton;
    }

    /**
     * Test for all view objects and activity to not be null.
     */
    public void testActivityFragmentViewObjectsAreNotNull() {
        assertNotNull(mainActivity);
        assertNotNull(convert);
        assertNotNull(convert.totalPriceText);
        assertNotNull(convert.numberOfOuncesText);
        assertNotNull(convert.pricePerOunceText);
        assertNotNull(convert.convertButton);
        assertNotNull(convert.clearButton);
    }

    /**
     * Check that the application performs a perfect instance price per ounce check
     */
    public void testValidConvert() {
        //create a loop for every a and b combo should give result c with varied input test validData
        String ppo = validData.pricePerOunce;

        for (String price : validData.price) {
            for (String ounces : validData.ounces) {
                boolean res = convert(price, ounces, ppo);
                assertTrue(res);
            }
        }
    }

    public void testInValidConvert() {
        //create a loop for every a and b combo should give result c with varied input test validData
        String ppo = invalidData.pricePerOunce;

        for (String price : invalidData.price) {
            for (String ounces : invalidData.ounces) {
                Log.d(TAG, "" + price + "/" + ounces);
                boolean res = convert(price, ounces, ppo);
                assertTrue(res);
            }
        }
    }

    public void testNonTerminatingData() {
        //create a loop for every a and b combo should give result c with varied input test validData
        String ppo = nonTerminatingData.pricePerOunce;

        for (String price : nonTerminatingData.price) {
            for (String ounces : nonTerminatingData.ounces) {
                Log.d(TAG, "" + price + "/" + ounces);
                boolean res = convert(price, ounces, ppo);
                assertTrue(res);
            }
        }
    }

    private boolean convert(String price, String ounces, String res) {
        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                clearButton.performClick();
            }
        });
        getInstrumentation().waitForIdleSync();

        // Set total
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                priceEditText.requestFocus();
            }
        });
        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync(price);
        getInstrumentation().waitForIdleSync();

        // Set ounces
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                totalOunces.requestFocus();
            }
        });
        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync(ounces);
        getInstrumentation().waitForIdleSync();

        // convert amount for price per ounce
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                convertButton.performClick();
            }
        });

        //check final amount to be correct
        getInstrumentation().waitForIdleSync();
        String pricePerOunce = pricePerOunceText.getText().toString();
        return (pricePerOunce.equals(res));
    }
}