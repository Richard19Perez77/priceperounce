package price.per.ounce;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

public class ApplicationClearEditTextTest extends
        ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;
    private PricePerOunce convert;

    PricePerOunceFragment fragment;

    public ApplicationClearEditTextTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        fragment = (PricePerOunceFragment) getActivity()
                .getSupportFragmentManager().findFragmentByTag(
                        "pricePerOunceTag");
        convert = fragment.convert;
    }

    /**
     * Test for all view objects and activity to not be null.
     */
    public void testActivityFragmentViewObjectsAreNotNull() {
        assertNotNull(mainActivity);
        assertNotNull(convert);
        assertNotNull(convert.totalPriceText);
        assertNotNull(convert.numberOfOuncesText);
        assertNotNull(convert.pricePerOunceText);
        assertNotNull(convert.convertButton);
        assertNotNull(convert.clearButton);
    }

    /**
     * Clear edit text field for total price make sure it does in fact clear. Next focus back into the text field and enter a valid digit. From here click the clear button and make sure the field is in fact clear.
     */
    public void testTotalPriceTextWriteAndClear() {
        final EditText totalPrice = convert.totalPriceText;
        final Button clearButton = convert.clearButton;

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                clearButton.performClick();
            }
        });

        getInstrumentation().waitForIdleSync();
        String cleared = convert.totalPriceText.getText().toString();
        assertTrue(cleared.equals(""));

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                totalPrice.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("10");
        getInstrumentation().waitForIdleSync();

        String ten = convert.totalPriceText.getText().toString();
        assertTrue(ten.equals("10"));

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                clearButton.performClick();
            }
        });

        getInstrumentation().waitForIdleSync();
        String empty = convert.totalPriceText.getText().toString();
        assertTrue(empty.equals(""));
    }

    /**
     * Clear edit text field for number of ounces field and make sure it does in fact clear. Next focus back into the text field and enter a valid digit. From here click the clear button and make sure the field is in fact clear.
     */
    public void testNumberOfOuncesTextWriteAndClear() {
        final EditText numberOfOunces = convert.numberOfOuncesText;
        final Button clearButton = convert.clearButton;

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                clearButton.performClick();
            }
        });

        getInstrumentation().waitForIdleSync();
        String cleared = convert.numberOfOuncesText.getText().toString();
        assertTrue(cleared.equals(""));

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                numberOfOunces.requestFocus();
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().sendStringSync("10");
        getInstrumentation().waitForIdleSync();

        String ten = convert.numberOfOuncesText.getText().toString();
        assertTrue(ten.equals("10"));

        // Send string input value
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                clearButton.performClick();
            }
        });

        getInstrumentation().waitForIdleSync();
        String empty = convert.numberOfOuncesText.getText().toString();
        assertTrue(empty.equals(""));
    }
}